﻿using System;
using System.Net;
using System.Linq;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using sx.yourstruly.nMVC.HTTP;
using sx.yourstruly.nMVC.Logging;
using sx.yourstruly.nMVC.Managers;
using sx.yourstruly.nMVC.Model;
using sx.yourstruly.nMVC.Model.Logger;
using sx.yourstruly.nMVC.Model.Settings;
using sx.yourstruly.nMVC.Model.HTTP;

namespace sx.yourstruly.NetcraveBot
{
	
	class WebController
	{
        public static Discord discord_bot;

        //static void Main(string[] args)
        //    => new WebController().MainAsync(args).GetAwaiter().GetResult();

        public static void Main(string[] args)
		{
            // Custom logger
			Logger._.Output += async (logData) => 
			{
				switch(logData.level)
				{
				case LogLevel.Debug:
					CustomDebugInfoLogging.HandleDebugLog(logData);
					break;
				default:
					if(SettingsManager<Settings>._.settings.debugging.AddTracingInfoInHTTPResponse)
					{
                            StackTrace st = new StackTrace();
                            string methodName = st.GetFrames()
                                .Take(3)
                                .Select(s => s.GetMethod().Name)
                                .Aggregate((c, n) => string.Format("{0}/{1}", c, n));

                            Console.WriteLine(string.Format("{0} : {1} : {2}", logData.level.ToString(), methodName, logData.message));
                            return;
					}
					Console.WriteLine(string.Format("{0} : {1}", logData.level.ToString(), logData.message));					
					break;
				}
			};

			Logger._.AddErrorLevel(LogLevel.Debug);
			Logger._.AddErrorLevel(LogLevel.Error);
			Logger._.AddErrorLevel(LogLevel.Info);
			Logger._.AddErrorLevel(LogLevel.Warn);
			Logger._.AddErrorLevel(LogLevel.Fatal);
           
            // Start Discord Bot
            discord_bot = new Discord();
            discord_bot.StartBot().ContinueWith(new Action<Task>((result) =>
            {
                Logger._.Info("Discord bot started");
            }));
           
           
            // Create web server and setup callbacks / routes
            WebServer wserv = new WebServer (
				WebController.HTTPRequestHandler, 
				SettingsManager<Settings>._.settings.HTTPListenerAddress);
			
			RESTCallbackManager._.RegisterGlobalCallbackHandlers();
            ContentManager._.PreloadContent();
           
            RouterManager._.Add(@"^/$", "index.html");

            RouterManager._.Add(@"^/.*\.(js)$", "js/");                                                        
            RouterManager._.Add(@"^/.*\.(css)$", "css/");
			RouterManager._.Add(@"^/.*\.(jpg|png)$", "images/");

            // These are only necesarry for API calls that the client can't/wont send the RESTKeys.RESTHandlerRequest (NMVC_API_V1_REQUEST)
            // TODO could refactor this, might re-write Router..
            RouterManager._.Add(@"/login$", "LOGIN");
            RouterManager._.Add(@"/login/$", "LOGIN");
            RouterManager._.Add(@"/validateauth$", "VALIDATEAUTH");
            RouterManager._.Add(@"/validateauth/$", "VALIDATEAUTH");
            RouterManager._.Add(@"/logout$", "LOGOUT");
            RouterManager._.Add(@"/v/$", "LOGOUT");


            // Start the webserver
            wserv.Run();

			Logger._.Info("Web application is running, press F10 to exit");

			readKey:
			ConsoleKeyInfo key = Console.ReadKey ();
			if(key.Key != ConsoleKey.F10)
				goto readKey;				
		}
        
		public static HttpResponse HTTPRequestHandler (HttpListenerContext ctx)
		{
            var identity = SessionManager<SessionIdentity>._.RetrieveSessionIdentity(ctx);
                     
            switch (ctx.Request.HttpMethod)
            {
                case "GET":
                    return WebController.HandleGETRequest (ctx, identity);

                case "POST":                    
                    return WebController.HandlePOSTRequest (ctx, identity);

                case "HEAD":
                    return HttpResponse.NewResponse();
                case "OPTIONS":
                    ctx.Response.AddHeader("Allow", "GET,HEAD,POST,OPTIONS");
                    return HttpResponse.NewResponse()
                    .AddHttpContext(ctx);

                case "PUT":
                case "DELETE":
                case "TRACE":            
                case "CONNECT":
                case "PATCH":
                default:
                    ctx.Response.StatusCode = (int)StatusCodes.HTTPStatus.HTTP_Method_Not_Allowed;
                    return HttpResponse.NewResponse()
                    .AddHttpContext(ctx);
			}
		}
		
        /// <summary>
        /// HTTP GET Callback 
        /// </summary>
        /// <param name="ctx"></param>
        /// <returns></returns>
		public static HttpResponse HandleGETRequest (HttpListenerContext ctx, ISessionIdentity identity)
		{           
            if (!string.IsNullOrEmpty (ctx.Request.Headers [RESTKeys.RESTHandlerRequest]))
			{               
                if (!RESTCallbackManager._.GETCallbacks.ContainsKey(ctx.Request.Headers [RESTKeys.RESTHandlerRequest]))
				{
                    return HttpResponse.NewResponse()
                        .AddStatusCode(StatusCodes.HTTPStatus.HTTP_Not_Found)
                        .AddHttpContext(ctx)
                        .RenderJSON();
				}
                
				Dictionary<string, string> Headers = UtilitiesManager._.GetHeaders(ctx);

                return RESTCallbackManager._.GETCallbacks
                    [ctx.Request.Headers[RESTKeys.RESTHandlerRequest]](Headers, identity)
                    .AddHttpContext(ctx)
                    .RenderJSON();                
			}

			else
			{
                string route = RouterManager._.FindRoute(ctx.Request.Url).FirstOrDefault();
                var split_route = route.Split(System.IO.Path.DirectorySeparatorChar).Last();
                if (!string.IsNullOrEmpty(route) && RESTCallbackManager._.GETCallbacks.ContainsKey(split_route))
                {
                    Dictionary<string, string> Headers = UtilitiesManager._.GetHeaders(ctx);
                    return RESTCallbackManager._.GETCallbacks[split_route](Headers, identity)
                        .AddHttpContext(ctx)
                        .Render();
                }
                else if (!string.IsNullOrEmpty(route) && ContentManager._.content.ContainsKey(route))
				{                    
                    return ContentManager._.content[route].HttpResponse()
                        .AddHttpContext(ctx)
                        .Render();
				}				
				else
				{
                    return HttpResponse.NewResponse()
                        .AddStatusCode(StatusCodes.HTTPStatus.HTTP_Not_Found)
                        .AddHttpContext(ctx)
                        .Render();                     
				}
			}
		}

		/// <summary>
        /// HTTP POST Callback
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
		public static HttpResponse HandlePOSTRequest (HttpListenerContext ctx, ISessionIdentity identity)
		{		
			// This is where the string specified in the REQUEST header is matched against the 
			// POST REST API mappings
			Dictionary<string, string> Headers = UtilitiesManager._.GetHeaders(ctx);
			Dictionary<string, string> PostData = UtilitiesManager._.GetPOSTData(ctx);
            string route = RouterManager._.FindRoute(ctx.Request.Url).FirstOrDefault();
            var split_route = (route != null) ? route.Split(System.IO.Path.DirectorySeparatorChar).Last() : null;

            if (!string.IsNullOrEmpty(split_route) && RESTCallbackManager._.POSTCallbacks.ContainsKey(split_route))
            {
                return RESTCallbackManager._.POSTCallbacks[split_route]
                    (Headers, identity, PostData)
                    .AddHttpContext(ctx)
                    .Render();
            }
            else if (!string.IsNullOrEmpty(ctx.Request.Headers[RESTKeys.RESTHandlerRequest]) && RESTCallbackManager._.POSTCallbacks.ContainsKey(ctx.Request.Headers[RESTKeys.RESTHandlerRequest]))
            {
                return RESTCallbackManager._.POSTCallbacks[ctx.Request.Headers[RESTKeys.RESTHandlerRequest]]
                    (Headers, identity, PostData)
                    .AddHttpContext(ctx)
                    .RenderJSON();
            }                
            else
            {                
                return HttpResponse.NewResponse().AddStatusCode(StatusCodes.HTTPStatus.HTTP_Not_Found)
                    .AddStatusCode(StatusCodes.HTTPStatus.HTTP_Not_Found)
                    .AddHttpContext(ctx)
                    .RenderJSON();                
            }                 			
		}
	}
}
