﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;

using sx.yourstruly.nMVC.Managers;
using sx.yourstruly.nMVC.Logging;

namespace sx.yourstruly.NetcraveBot
{    
    public class Discord
    {
        private DiscordSocketClient _client;       
        private List<Guild> guilds;
        
        public Discord()
        {
            guilds = new List<Guild>();
          
            _client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                DefaultRetryMode = RetryMode.AlwaysRetry,
                LogLevel = LogSeverity.Debug,
            });
            
            _client.Ready += _client_Ready;
            _client.Connected += _client_Connected;
            _client.Disconnected += _client_Disconnected;
            _client.LoggedIn += _client_LoggedIn;
            _client.LoggedOut += _client_LoggedOut;
            _client.MessageReceived += _client_MessageReceived;
            _client.Log += _client_Log;
            _client.GuildAvailable += _client_GuildAvailable;            
        }
        
        private Task _client_GuildAvailable(SocketGuild arg)
        {
            if(guilds.Where(w => w.guild.Name == arg.Name).Count() > 0)
            {
                return Task.CompletedTask;
            }

            var guild = new Guild(arg);
            guilds.Add(guild);          
            return Task.CompletedTask;
        }

        private Task _client_Ready()
        {
            Logger._.Info("Discord client ready");
            return Task.CompletedTask;
        }

        public Task StartBot()
        {
            return (_client.LoginAsync(TokenType.Bot, SettingsManager<BotSettings>._.settings.dc_client.BotLogin, true)
            .ContinueWith(new Action<Task>((result) =>
            {
                if (result.IsFaulted)
                    Logger._.Error(UtilitiesManager._.PrettyRenderException(result.Exception));

                _client.StartAsync().ContinueWith(new Action<Task>((result2) =>
                {
                    if (result2.IsFaulted)
                        Logger._.Error(UtilitiesManager._.PrettyRenderException(result2.Exception));
                }));
            })));            
        }

        public string GetStatus()
        {
            return _client.ConnectionState.ToString();
        }

        public string[] GetAvailableGuilds()
        {
            return _client.Guilds.Select(s => s.Name).ToArray();
        }

        private Task _client_Log(LogMessage arg)
        {
            Logger._.Info(string.Format("{0} : {1}", arg.Source, arg.Message));
            return Task.CompletedTask;
        }

        private Task _client_MessageReceived(SocketMessage arg)
        {
            if (arg.Author.Username == _client.CurrentUser.Username)
                return Task.CompletedTask;
            var guild = guilds.Where(w => w.guild.Name == ((SocketTextChannel)arg.Channel).Guild.Name).Single();
            guild.InboundChannelMessage(arg);
            return Task.CompletedTask;
        }

        private Task _client_LoggedOut()
        {            
            Logger._.Info(string.Format("Discord Logged out {0} : {1}", 
                _client.LoginState.ToString(), 
                _client.ConnectionState.ToString()
                //_client.CurrentUser.Username
                ));
            return Task.CompletedTask;
        }

        private Task _client_LoggedIn()
        {
            Logger._.Info("Logged into Discord");
            return Task.CompletedTask;
        }

        private Task _client_Disconnected(Exception arg)
        {
            Logger._.Info(arg.Message);
            return Task.CompletedTask;
        }

        private Task _client_Connected()
        {
            Logger._.Info("Connected to Discord");
            return Task.CompletedTask;
        }
    }
}
