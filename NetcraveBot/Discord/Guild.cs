﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;

using sx.yourstruly.nMVC.Logging;

namespace sx.yourstruly.NetcraveBot
{
    public class Guild
    {
        public readonly SocketGuild guild;
        public BotUser[] users = new BotUser[0];
        public DiscordRelay relay;

        public Guild(SocketGuild guild)
        {           
            this.guild = guild;           
        }
        
        private Task ParseInboundMessage(SocketMessage message)
        {
           
            return Task.CompletedTask;
        }

        public Task InboundChannelMessage(SocketMessage message)
        {
            return ParseInboundMessage(message);
        }

        public Task InboundPrivateMessage(SocketMessage message)
        {
            return ParseInboundMessage(message);
        }
    }
}