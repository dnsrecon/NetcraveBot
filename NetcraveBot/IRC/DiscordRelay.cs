﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using IrcDotNet;

using sx.yourstruly.nMVC.Managers;
using sx.yourstruly.nMVC.Logging;

namespace sx.yourstruly.NetcraveBot
{
    public class DiscordRelay : BasicIrcBot
    {
        public IRCNetwork settings;
        private Guild guild;


        public DiscordRelay(Guild guild, IRCNetwork settings)
            : base()
        {
            this.guild = guild;
            this.settings = settings;            
        }

        public override IrcRegistrationInfo RegistrationInfo
        {
            get
            {
                return new IrcUserRegistrationInfo()
                {
                    NickName = settings.NickName,
                    UserName = settings.UserName,
                    RealName = settings.RealName,
                    Password = settings.Password                                      
                };
            }
        }

        public override string QuitMessage
        {
            get
            {
                return "https://github.com/netcrave/NetcraveBot";
            }
        }
     
        public Task Connect()
        {
            this.Connect(settings.Server, settings.Port, settings.SSL, this.RegistrationInfo);
            return Task.CompletedTask;
        }

        protected override void OnClientConnect(IrcClient client)
        {
            Logger._.Info(string.Format("{0} connected {1} port {2} {3}", 
                this.ToString(), 
                settings.Server, 
                settings.Port, 
                ((settings.SSL) ? "SSL" : "")));
        }

        protected override void OnClientDisconnect(IrcClient client)
        {
            this.Connect(settings.Server, settings.Port, settings.SSL, this.RegistrationInfo);
        }

        protected override void OnClientRegistered(IrcClient client)
        {
            //
        }

        protected override void OnLocalUserJoinedChannel(IrcLocalUser localUser, IrcChannelEventArgs e)
        {
            
        }

        protected override void OnLocalUserLeftChannel(IrcLocalUser localUser, IrcChannelEventArgs e)
        {
            //
        }

        protected override void OnLocalUserNoticeReceived(IrcLocalUser localUser, IrcMessageEventArgs e)
        {
            //
        }

        protected override void OnLocalUserMessageReceived(IrcLocalUser localUser, IrcMessageEventArgs e)
        {
            //
        }

        protected override void OnChannelUserJoined(IrcChannel channel, IrcChannelUserEventArgs e)
        {
            //
        }

        protected override void OnChannelUserLeft(IrcChannel channel, IrcChannelUserEventArgs e)
        {
            //
        }

        protected override void OnChannelNoticeReceived(IrcChannel channel, IrcMessageEventArgs e)
        {
            //
        }

        protected override void OnChannelMessageReceived(IrcChannel channel, IrcMessageEventArgs e)
        {
           //
        }

        protected override void InitializeChatCommandProcessors()
        {
            base.InitializeChatCommandProcessors();            
            this.ChatCommandProcessors.Add("talk", ProcessChatCommandTalk);
        }

        #region Chat Command Processors

        private void ProcessChatCommandTalk(IrcClient client, IIrcMessageSource source,
            IList<IIrcMessageTarget> targets, string command, IList<string> parameters)
        {
         
        }
        
        #endregion

   
        protected override void InitializeCommandProcessors()
        {
            base.InitializeCommandProcessors();
        }

        #region Command Processors

        //

        #endregion
    }
}