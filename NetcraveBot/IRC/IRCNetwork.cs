﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sx.yourstruly.NetcraveBot
{
    /// <summary>
    /// Settings class
    /// </summary>
    public class IRCNetwork
    {
        public string NickName = "Netcrave";
        public string AltNickName = "Netcrave_";
        public string UserName = "Netcrave";
        public string RealName = "Netcrave Bot";
        public string Server = "irc.blessed.net";
        public string Password = "";
        public int Port = 6697;
        public string[] channels = new string[] { "#netcrave" };
        public bool SSL = true;
        public IRCNetwork()
        {

        }
    }
}
