﻿using System;
using System.Collections.Generic;
using System.Text;
using sx.yourstruly.nMVC.Model.Settings;
using sx.yourstruly.nMVC.Managers;

namespace sx.yourstruly.NetcraveBot 
{
    public class BotSettings : ISettings
    {
        public DiscordClient dc_client = new DiscordClient();
        public IRCNetwork[] networks = new IRCNetwork[] { new IRCNetwork() };

        public void Save(string filename)
        {
            UtilitiesManager._.SaveObjectToXML<BotSettings>(filename, this);
        }

        public ISettings Load(string filename)
        {
            return UtilitiesManager._.LoadObjectFromXML<BotSettings>(filename);
        }
    }
}
