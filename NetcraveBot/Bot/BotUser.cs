﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sx.yourstruly.NetcraveBot
{
    public class BotUser
    {
        public string UserName;
        public BotPrivilege[] privileges = new BotPrivilege[0];
        public BotUserGroup[] groups = new BotUserGroup[0];

    }
}
