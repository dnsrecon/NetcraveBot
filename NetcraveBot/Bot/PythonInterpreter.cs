﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;

using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using IronPython.Hosting;

using Discord;
using Discord.WebSocket;
using sx.yourstruly.nMVC.Logging;

namespace SampleProject
{
    public class PythonInterpreter
    {
        private ScriptEngine m_engine = Python.CreateEngine();
        private ScriptScope m_scope = null;
        MemoryStream ms = new MemoryStream();
        EventRaisingStreamWriter outputWr;
        public PythonInterpreter()
        {
            outputWr = new EventRaisingStreamWriter(ms);
            outputWr.StringWritten += OutputWr_StringWritten;
            m_scope = m_engine.CreateScope();
            m_engine.Runtime.IO.SetOutput(ms, outputWr);
            m_scope.SetVariable("current_guild", this);
        }

        private void OutputWr_StringWritten(object sender, MyEvtArgs<string> e)
        {
            Logger._.Info(e.Value);
        }

        public void RunPythonCode(SocketMessage message)
        {
            Regex regex = new Regex("```(.*)```");
            var matches = regex.Match(message.Content);
            string source = matches.Groups[1].ToString();
            var sourceCode = m_engine.CreateScriptSourceFromString(source);
            sourceCode.Execute(m_scope);
            return;
        }

    }

    public class MyEvtArgs<T> : EventArgs
    {
        public T Value
        {
            get;
            private set;
        }
        public MyEvtArgs(T value)
        {
            this.Value = value;
        }
    }

    public class EventRaisingStreamWriter : StreamWriter
    {
        #region Event
        public event EventHandler<MyEvtArgs<string>> StringWritten;
        #endregion

        #region CTOR
        public EventRaisingStreamWriter(Stream s) : base(s)
        { }
        #endregion

        #region Private Methods
        private void LaunchEvent(string txtWritten)
        {
            if (StringWritten != null)
            {
                StringWritten(this, new MyEvtArgs<string>(txtWritten));
            }
        }
        #endregion
    }
}
