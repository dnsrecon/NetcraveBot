﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using RiakClient;
using RiakClient.Config;
using sx.yourstruly.nMVC.Managers;
using sx.yourstruly.nMVC.Model.Settings;
using System.Xml;
using System.Xml.Serialization;

namespace sx.yourstruly.NetcraveBot
{
    public class DBSettings : ISettings
    {
        [XmlIgnore]
        public RiakClient.Config.RiakClusterConfiguration cluster;      
        public RiakNodeConfiguration[] nodes = new RiakNodeConfiguration[] { new RiakNodeConfiguration() };
        public DBSettings()
        {            
            var node_collection = new RiakClient.Config.RiakNodeConfigurationCollection();
            this.nodes.ToList().ForEach(f => 
            {
                node_collection.Add(new RiakClient.Config.RiakNodeConfiguration()
                {
                    HostAddress = f.HostAddress,
                    PbcPort = f.PbcPort,
                    Name = f.Name
                });
            });
            this.cluster = new RiakClusterConfiguration()
            {
                Nodes = node_collection
            };
        }                  

        public ISettings Load(string FileName)
        {
            return UtilitiesManager._.LoadObjectFromXML<DBSettings>(FileName);
        }

        public void Save(string FileName)
        {
            UtilitiesManager._.SaveObjectToXML<DBSettings>(FileName, this);
        }
    }
}
