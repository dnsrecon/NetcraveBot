﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using sx.yourstruly.nMVC.Model.Settings;
using sx.yourstruly.nMVC.Managers;

namespace sx.yourstruly.NetcraveBot
{
    public class APISettings : ISettings
    {
        public string Auth0Domain = "https://";
        public string Auth0ClientId = "";
        public string Auth0ClientSecret = "";

        public ISettings Load(string FileName)
        {
            return UtilitiesManager._.LoadObjectFromXML<APISettings>(FileName);
        }

        public void Save(string FileName)
        {
            UtilitiesManager._.SaveObjectToXML<APISettings>(FileName, this);
        }
    }
}
