﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

using sx.yourstruly.nMVC.Model;
using sx.yourstruly.nMVC.Model.HTTP;
using sx.yourstruly.nMVC.Managers;
using sx.yourstruly.nMVC.Logging;

using Auth0.Core;
using Auth0.Core.Http;
using Auth0.Core.Collections;
using Auth0.Core.Exceptions;
using Auth0.Core.Serialization;
using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Auth0.AuthenticationApi.Builders;

namespace sx.yourstruly.NetcraveBot.nMVC
{
    [RESTService()]
    class API : Manager
    {
        private static API instance;
        private AuthenticationApiClient auth_client;

        private API()
        {
            // http://auth0.github.io/auth0.net/
            auth_client = new AuthenticationApiClient(new Uri(SettingsManager<APISettings>._.settings.Auth0Domain));

            //new Auth0.ManagementApi.ManagementApiClient()
        }

        public static new API Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new API();
                }

                return instance;
            }
        }

        [RESTContract(RESTContractAttribute.RequestType.GET, RESTKeys.RESTHandlerRequest, "DISCORD_STATUS")]
        public HttpResponse GetDiscordStatus(Dictionary<string, string> headers, ISessionIdentity identity)
        {
            return HttpResponse.NewResponse().AddData(WebController.discord_bot.GetStatus());
        }

        [RESTContract(RESTContractAttribute.RequestType.GET, RESTKeys.RESTHandlerRequest, "AVAILABLE_GUILDS")]
        public HttpResponse GetAvailableGuilds(Dictionary<string, string> headers, ISessionIdentity identity)
        {
            return HttpResponse.NewResponse().AddData(WebController.discord_bot.GetAvailableGuilds());
        }

        [RESTExpectsParam("EMAIL", RESTExpectsParam.ParamType.POST)]
        [RESTExpectsParam("PASSWORD", RESTExpectsParam.ParamType.POST)]
        [RESTExpectsParam("GUILDCLAIMCODE", RESTExpectsParam.ParamType.POST)]
        [RESTContract(RESTContractAttribute.RequestType.POST, RESTKeys.RESTHandlerRequest, "REGISTER_ACCOUNT")]
        public HttpResponse RegisterAccount(Dictionary<string, string> headers, ISessionIdentity identity, Dictionary<string, string> postData)
        {
            var response = HttpResponse.NewResponse();
            var request = new Auth0.AuthenticationApi.Models.SignupUserRequest();

            request.ClientId = SettingsManager<APISettings>._.settings.Auth0ClientId;
            request.Connection = "Username-Password-Authentication";
            request.Email = postData["EMAIL"];
            request.Password = postData["PASSWORD"];

            auth_client.SignupUserAsync(request).ContinueWith(new Action<Task<SignupUserResponse>>(async (result) =>
            {
                try
                {
                    if (result.IsFaulted)
                    {
                        Logger._.Info(UtilitiesManager._.PrettyRenderException(result.Exception));
                    }
                    var data = await result;
                    response.AddData(data);
                }
                catch (Exception ex)
                {
                    response.AddStatusCode(StatusCodes.HTTPStatus.HTTP_Internal_Server_Error);
                    response.AddData(Newtonsoft.Json.JsonConvert.DeserializeObject(ex.Message));
                }
            })).Wait();

            return response;
        }

        /// <summary>
        /// Authenticate using Auth0 / URL Builder 
        /// Creates a nonce and stores it in the session, then redirects the client to Auth0's login page and upon authentication is redirected back to here with a 
        /// token. The nonce is used to mitigate token replay attacks.
        /// </summary>
        /// <param name="headers"></param>
        /// <param name="identity"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        [RESTContract(RESTContractAttribute.RequestType.GET, RESTKeys.RESTHandlerRequest, "LOGIN")] // BUG: CallbackManager fails (exception) if you dont specify this, should warn/error
                                                                                                    // or info, since its not really an error just a public method that won't be bound as an API call.
        public HttpResponse Login(Dictionary<string, string> headers, ISessionIdentity identity)
        {
            var nonce = new Auth0Nonce(identity);
            identity.StoreStateObject<Auth0Nonce>(nonce);

            var redirect_to_login_url = new AuthorizationUrlBuilder(SettingsManager<APISettings>._.settings.Auth0Domain)
                .WithClient(SettingsManager<APISettings>._.settings.Auth0ClientId)
                .WithResponseType(AuthorizationResponseType.IdToken, AuthorizationResponseType.Token)
                .WithResponseMode(AuthorizationResponseMode.FormPost)
                .WithNonce(nonce.ToString())
                .WithRedirectUrl(string.Format("{0}validateauth", SettingsManager<sx.yourstruly.nMVC.Model.Settings.Settings>._.settings.HTTPListenerAddress.Aggregate((a, b) => a + ":" + b)))
                .Build().ToString();

            Logger._.Info(string.Format("auth0 query built {0}", redirect_to_login_url));
            return HttpResponse.NewResponse()
                .AddRedirect(redirect_to_login_url)
                .AddStatusCode(StatusCodes.HTTPStatus.HTTP_Moved_Temporarily);
        }

        [RESTContract(RESTContractAttribute.RequestType.POST, RESTKeys.RESTHandlerRequest, "VALIDATEAUTH")] // BUG: CallbackManager fails (exception) if you dont specify this, should warn/error
        public HttpResponse Auth0Callback(Dictionary<string, string> headers, ISessionIdentity identity, Dictionary<string, string> postData)
        {
            var ret = HttpResponse.NewResponse();
            var client = new AuthenticationApiClient(new Uri(SettingsManager<APISettings>._.settings.Auth0Domain));
            if(postData["ACCESS_TOKEN"] == null)
            {
                var u = identity.GetStateObject<UserState>();
                if (u != null)
                    identity.DeleteStateObject<UserState>(u);                                     
            }
            var user = client.GetUserInfoAsync(postData["ACCESS_TOKEN"]);

            user.ContinueWith(new Action<Task<UserInfo>>((result) =>
            {
                if (result.IsFaulted)
                {
                    ret.AddStatusCode(StatusCodes.HTTPStatus.HTTP_Internal_Server_Error);
                }
                else
                {
                    identity.SetMaxLifeTime();
                    UserState u = UserState.NewState(identity, true, client);
                    u.UserLoginInfo = user.Result;
                    u.AccessToken = postData["ACCESS_TOKEN"];
                    identity.StoreStateObject<UserState>(u);
                    ret.AddRedirect("/").AddStatusCode(StatusCodes.HTTPStatus.HTTP_Moved_Temporarily);
                }
            })).Wait();

            return ret;
        }

        [RESTContract(RESTContractAttribute.RequestType.GET, RESTKeys.RESTHandlerRequest, "USERSTATE")] // BUG: CallbackManager fails (exception) if you dont specify this, should warn/error
        public HttpResponse GetUserState(Dictionary<string, string> headers, ISessionIdentity identity)
        {
            var u = identity.GetStateObject<UserState>();         
            return HttpResponse.NewResponse().AddData(u);
        }

        [RESTContract(RESTContractAttribute.RequestType.GET, RESTKeys.RESTHandlerRequest, "LOGOUT")] // BUG: CallbackManager fails (exception) if you dont specify this, should warn/error
        public HttpResponse LogOut(Dictionary<string, string> headers, ISessionIdentity identity)
        {
            identity.Delete();
            return HttpResponse.NewResponse().AddRedirect("/").AddStatusCode(StatusCodes.HTTPStatus.HTTP_Moved_Temporarily);
        }
    }
}
