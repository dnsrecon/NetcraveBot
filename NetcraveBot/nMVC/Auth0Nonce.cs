﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

using sx.yourstruly.nMVC.Model.HTTP;

namespace sx.yourstruly.NetcraveBot
{
    public class Auth0Nonce : IStateObject
    {
        private readonly Guid Id = Guid.NewGuid();
        private readonly string nonce;
        private readonly ISessionIdentity identity;
        
        public Auth0Nonce(ISessionIdentity identity)
        {
            this.identity = identity;
            nonce = HexStringFromBytes(new SHA256CryptoServiceProvider().ComputeHash(GetRandomBytes(1000 * 1024)));
        }

        public bool Equals(IStateObject state)
        {
            if (this.HashCode() == state.HashCode())
                return true;
            return false;
        }

        public ISessionIdentity GetSessionIdentity()
        {
            return this.identity;
        }

        public int HashCode()
        {
            return this.Id.GetHashCode();
        }

        public override string ToString()
        {
            return nonce;
        }
        private byte[] GetRandomBytes(int count)
        {
            var bytes = new byte[count];
            (new Random()).NextBytes(bytes);
            return bytes;
        }
        private string HexStringFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            return sb.ToString();
        }
    }
}
