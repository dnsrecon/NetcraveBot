﻿using System;

using sx.yourstruly.nMVC.Model.Logger;
using sx.yourstruly.nMVC.Extensions;
using sx.yourstruly.nMVC.Model.HTTP;

namespace sx.yourstruly.NetcraveBot
{
	public static class CustomDebugInfoLogging
	{
		public static void HandleDebugLog(LogData data) 
		{
			//[0]	{{ data = de.netcrave.nMVC.RESTContractAttribute, t = SampleProject.AccountService, mi = de.netcrave.nMVC.HttpResponse AuthenticateFromLocal(System.Collections.Generic.Dictionary`2[System.String,System.String], SampleProject.CustomSessionIdentity, System.Collections.Generic.Dictionary`2[System.String,System.String]) }}	<>__AnonType1<de.netcrave.nMVC.RESTContractAttribute,System.Type,System.Reflection.MethodInfo>
			foreach(object obj in data.debugging)
			{
				if(obj.GetType().GetProperty("data") != null
					&& obj.GetType().GetProperty("t") != null
					&& obj.GetType().GetProperty("mi") != null)
				{
					var debugData = obj.ToDynamic();
					LogRestCallBackHandler(data, debugData.data, debugData.t, debugData.mi);
				}
			}
		}

		/// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="rest_contract_attribute"></param>
        /// <param name="type_info"></param>
        /// <param name="method_info"></param>
		private static void LogRestCallBackHandler(LogData data, object rest_contract_attribute, object type_info, object method_info)
		{
			if(rest_contract_attribute.GetType() == typeof(RESTContractAttribute))
			{
				var contractAttr = (RESTContractAttribute) rest_contract_attribute;
				var t = (System.Reflection.TypeInfo) type_info;
				var mi = (System.Reflection.MethodInfo) method_info;

				Console.WriteLine(data.level.ToString() 
					+ " : " 
					+ data.message
					+ " : "
					+ contractAttr.APILevel
					+ " : "
					+ t.Name
					+ " : "
					+ mi.Name
				);
			}
		}
	}
}

