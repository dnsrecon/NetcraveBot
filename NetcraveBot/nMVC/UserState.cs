﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Serialization;
using Newtonsoft.Json;

using sx.yourstruly.nMVC.Model.HTTP;
using sx.yourstruly.nMVC.Model;
using sx.yourstruly.nMVC.Model.Settings;
using sx.yourstruly.nMVC.Model.DataAccess;
using sx.yourstruly.nMVC.Managers;

using RiakClient;
using RiakClient.Config;

using Auth0.AuthenticationApi.Models;
using Auth0.AuthenticationApi.Builders;
using Auth0.AuthenticationApi;


namespace sx.yourstruly.NetcraveBot.nMVC
{
    class UserState : DomainObjectBase, IStateObject
    {
        readonly Guid Id = Guid.NewGuid();

        public string Email;
        public string AccessToken;

        [JsonIgnore]
        [XmlIgnore]
        public UserInfo UserLoginInfo;     
        public readonly bool Authenticated;

        [JsonIgnore]
        [XmlIgnore]
        private readonly ISessionIdentity Identity;
        
        public string LogoutURL
        {
            get
            {

                return _LogoutURL;
            }
        }

        private string _LogoutURL = "/logout";

        public override DomainObjectBase Create()
        {
            return (new RiakClient.RiakCluster(SettingsManager<DBSettings>._.settings.cluster)
                .CreateClient()
                .Put(new RiakClient.Models.RiakObject("Users", this.Email, this))
                .IsSuccess) ? this : null;            
        }

        private UserState(ISessionIdentity identity, bool authenticated, AuthenticationApiClient client)
        {
            // Federated clients cannot access non-authority tenants.
            // TRACKING ID: d0bb6bc60f94ed2e5611         
            /*_LogoutURL = client.BuildLogoutUrl()
                .WithClientId(SettingsManager<APISettings>._.settings.Auth0Domain)
                .WithReturnUrl(
                string.Format("{0}validateauth",
                SettingsManager<sx.yourstruly.nMVC.Model.Settings.Settings>._
                .settings.HTTPListenerAddress
                .Aggregate((a, b) => a + ":" + b)))
                .Build().ToString();*/
            
            this.Identity = identity;
            this.Authenticated = authenticated;
        }

        public static UserState NewState(ISessionIdentity identity, bool authenticated, AuthenticationApiClient client)
        {
            return new UserState(identity, authenticated, client);
        }

        public static UserState Retrieve(string email)
        {
            return new RiakClient.RiakCluster(SettingsManager<DBSettings>._.settings.cluster)
               .CreateClient()
               .Get(new RiakClient.Models.RiakObjectId("Users", email))
               .Value
               .GetObject<UserState>();
        }

        public override BackendQueryStatus.ReturnCode Delete()
        {
            return (new RiakClient.RiakCluster(SettingsManager<DBSettings>._.settings.cluster)
               .CreateClient()
               .Delete(new RiakClient.Models.RiakObject("Users", this.Email))
               .IsSuccess) 
               ? BackendQueryStatus.ReturnCode.Success 
               : BackendQueryStatus.ReturnCode.Failure;
        }

        public bool Equals(IStateObject state)
        {
            if (state.HashCode() == this.HashCode())
                return true;
            return false;
        }        

        public int HashCode()
        {
            return Id.GetHashCode();
        }

        public override BackendQueryStatus.ReturnCode Rollback()
        {
            throw new NotImplementedException();
        }

        public override BackendQueryStatus.ReturnCode Save()
        {
            throw new NotImplementedException();
        }

        public override BackendQueryStatus.ReturnCode Update()
        {
            throw new NotImplementedException();
        }

        public ISessionIdentity GetSessionIdentity()
        {
            return this.Identity;
        }
    }
}
