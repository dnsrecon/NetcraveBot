﻿using System;
using System.Collections.Generic;
using System.Text;

using sx.yourstruly.nMVC.Model.HTTP;

namespace sx.yourstruly.NetcraveBot.nMVC
{
    class GuildState : IStateObject
    {
        private ISessionIdentity identity; 

        public GuildState(ISessionIdentity identity)
        {
            this.identity = identity;
            identity.StoreStateObject<GuildState>(this);
        }
        
        public ISessionIdentity GetSessionIdentity()
        {
            return identity;
        }

        public bool Equals(IStateObject obj)
        {
           if(obj.HashCode() == this.HashCode())
            {
                return true;
            }
            return false;
        }

        public int HashCode()
        {
            return this.identity.HashCode();
        }

        // Implement Guild claim 
    }
}
