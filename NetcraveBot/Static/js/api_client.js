﻿function nMVCAPIClient(api_method) {
    this.APIMethod = api_method;
    this.xmlhttp = new XMLHttpRequest();
    this.URL = "/";
    
    this.OnSuccess = function (callback) {
        if (callback != null)
            this.Success = callback;
        else
            this.Success = function (status, response) { console.log(status + " " + response); }

        return this;
    };

    this.OnError = function (callback) {
        if (callback != null)
            this.Error = callback;
        else
            this.Error = function (status, response) { console.log(status + " " + response); }

        return this;
    };

    this.xmlhttp.APIClient = this;
    
    this.xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            this.APIClient.Success(this.status, this.response);
        }
        else {
            this.APIClient.Error(this.status, this.response);
        }        
    };

    this.GET = function () {           
        this.xmlhttp.open("GET", this.URL, true);
        this.xmlhttp.setRequestHeader("NMVC_API_V1_REQUEST", this.APIMethod);
        this.xmlhttp.responseType = "json";
        this.xmlhttp.send();

        return this;
    };

    this.POST = function () {
        this.xmlhttp.open("POST", this.URL, true);
        this.xmlhttp.setRequestHeader("NMVC_API_V1_REQUEST", this.APIMethod);
        this.xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        this.xmlhttp.responseType = "json";
        this.xmlhttp.send(this.PostData);

        return this;
    };

    this.SetPostData = function (data) {
        this.PostData = data;

        return this;
    }
}
