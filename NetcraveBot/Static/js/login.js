﻿document.getElementById("login-form")
    .addEventListener("submit", function (sender, event) {
        var postData = "";
        for (var index = 0; index < sender.target.length; index++) {
            postData += sender.target[index].name + "=" + sender.target[index].value + "&";
        }
        new nMVCAPIClient("LOGIN")
            .OnSuccess(function (status, data) {
                if (data != null && data.Messages != null) {
                    document.getElementsByClassName("signin-form")[0].innerHTML = atob(data.Messages[1]);
                    // = document.getElementsByClassName("signin-form")[0].children[0][2].value.replace(/\"/g, "'");
                    document.getElementsByClassName("signin-form")[0].children[0].submit();
                }
            })
            .OnError(function (status, error) {
                if (error != null && error.data != undefined) {
                    document.getElementsByClassName("form_description")[0].innerHTML = "<p>" + error.data[0].message + "</p>";
                    document.getElementsByClassName("form_description")[0].innerHTML += "<p>" + error.data[0].policy + "</p>";
                }
                else if (error != null && error.Messages != null) {
                    document.getElementsByClassName("form_description")[0].innerHTML = "";
                    for (var index = 0; index < error.Messages.length; index++) {
                        document.getElementsByClassName("form_description")[0].innerHTML += "<p>" + error.Messages[index] + "</p>";
                    }
                }
            })
            .SetPostData(postData)
            .POST();
        sender.preventDefault();
    });