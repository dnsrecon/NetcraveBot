﻿(function () {
    document.getElementById("register-form")
        .addEventListener("submit", function (sender, event) {
            var postData = "";
            for (var index = 0; index < sender.target.length; index++) {
                postData += sender.target[index].name + "=" + sender.target[index].value + "&";
            }
            new nMVCAPIClient("REGISTER_ACCOUNT")
                .OnSuccess(function (status, data) {
                    if (data[0] != null && data[0].email_verified != null)
                        document.getElementsByClassName("form_description2")[0].innerHTML = "<p>Thanks! you need to verify by e-mail before you can login</p>";
                })
                .OnError(function (status, error) {
                    if (error != null && error.data != undefined) {
                        document.getElementsByClassName("form_description2")[0].innerHTML = "<p>" + error.data[0].message + "</p>";
                        document.getElementsByClassName("form_description2")[0].innerHTML += "<p>" + error.data[0].policy + "</p>";
                    }
                })
                .SetPostData(postData)
                .POST();

            sender.preventDefault();
        });
})();