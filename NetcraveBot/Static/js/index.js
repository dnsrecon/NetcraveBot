﻿(function () {
    function IsAuthenticated() {
        var user = Window.sessionStorage.getItem("User");
        //if (if user != undefined && user.)
    }
    new nMVCAPIClient("DISCORD_STATUS")
        .OnSuccess(function (status, data) {
            document.getElementsByClassName("discord-status-span")[0].innerText = data.data[0];
            console.log(data);
        })
        .OnError(function (status, error) {
            console.log(error);
        })
        .GET();

    new nMVCAPIClient("AVAILABLE_GUILDS")
        .OnSuccess(function (status, data) {
            for (var index = 0; index < data.data.length; index++) {
                document.getElementsByClassName("available-guilds-list")[0].innerHTML += "<li>" + data.data[index] + "</li>";
            }
            console.log(data);
        })
        .OnError(function (status, error) {
            console.log(error);
        })
        .GET();

    new nMVCAPIClient("USERSTATE")
        .OnSuccess(function (status, data) {
            var User = data.data[0]; 
            if (User.Authenticated) {
                document.getElementsByClassName("links")[0].innerHTML = "<a href=\"" + User.LogoutURL + "\">Logout</a>";
            }
            console.log(data);
        })
        .OnError(function (status, error) {
            console.log(error);
        })
        .GET();    
})();