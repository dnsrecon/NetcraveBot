# Screenshot
![alt text](https://github.com/netcrave/NetcraveBot/raw/master/screenshots/1.png "screenshot")
 
# Configuration examples
Place the configuration files in the same directory as the executable. Alternatively you can run the bot and then stop it and then configuration file defaults will 
be generated and placed in the bin/ directory for you.

# Windows 
You need a urlacl to listen on anything besides localhost: 

```
C:\Windows\system32>netsh http add urlacl url="http://10.242.0.107:80/" user=everyone

URL reservation successfully added
```

### sx.yourstruly.NetcraveBot.DBSettings.xml
```
<?xml version="1.0" encoding="utf-8"?>
<DBSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <nodes>
    <RiakNodeConfiguration>
      <HostAddress>172.17.0.2</HostAddress>
      <PbcPort>8087</PbcPort>
      <Name>default</Name>
    </RiakNodeConfiguration>
  </nodes>
</DBSettings>
```

### sx.yourstruly.NetcraveBot.APISettings.xml
```
<?xml version="1.0" encoding="utf-8"?>
<APISettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Auth0Domain>https://paigeat.auth0.com</Auth0Domain>
  <Auth0ClientId>id</Auth0ClientId>
  <Auth0ClientSecret>scret</Auth0ClientSecret>
</APISettings>
```

### sx.yourstruly.NetcraveBot.BotSettings.xml
```
<?xml version="1.0" encoding="utf-8"?>
<BotSettings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <dc_client>
    <WebHookId>0</WebHookId>
    <WebHookToken>none</WebHookToken>
    
<BotLogin>NDY4MDQwNjA0ODI0MTA5MDU2.Di0lNQ.G04LS2cITu25hXzrUnJ8MJKA2xI</BotLogin>
  </dc_client>
  <networks>
    <IRCNetwork>
      <NickName />
      <AltNickName />
      <UserName />
      <RealName />
      <Server />
      <Port>0</Port>
      <channels>
        <string />
      </channels>
      <SSL>true</SSL>
    </IRCNetwork>
  </networks>
</BotSettings>

```
### sx.yourstruly.nMVC.Model.Settings.Settings.xml
```
<?xml version="1.0" encoding="utf-8"?>
<Settings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  
<DefaultMinSessionLifeTimeSeconds>120</DefaultMinSessionLifeTimeSeconds>
  
<DefaultMaxSessionLifeTimeSeconds>3600</DefaultMaxSessionLifeTimeSeconds>
  
<InProcessSessionBackingStoreSweepInterval>60</InProcessSessionBackingStoreSweepInterval>
  <HTTPListenerAddress>
    <string>http://localhost:8000/</string>
  </HTTPListenerAddress>
  <HTTPWorkerThreads>2</HTTPWorkerThreads>
  <HTTPCompletionPortThreads>2</HTTPCompletionPortThreads>
  <HtdocRoot>HTML/</HtdocRoot>
  <HTTPServerHeader>nMVC-1.0.0.0</HTTPServerHeader>
  <debugging>
    <PrettyPrintJSONResponse>true</PrettyPrintJSONResponse>
    <AddTracingInfoInHTTPResponse>true</AddTracingInfoInHTTPResponse>
    <EnableMessagesInHTTPResponse>true</EnableMessagesInHTTPResponse>
    <EnableExceptions>true</EnableExceptions>
  </debugging>
</Settings>
```
